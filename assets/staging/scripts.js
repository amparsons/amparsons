// Avoid console errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
	}
	
}());

jQuery(document).ready(function($){
	
	//open navigation clicking the menu icon
	$('.cd-nav-trigger').on('click', function(event){
		event.preventDefault();
		toggleNav(true);
		
	});
	//close the navigation
	$('.cd-close-nav').on('click', function(event){
		event.preventDefault();
		toggleNav(false);
	});

	function toggleNav(bool) {
		$('.cd-nav-container, .cd-nav-trigger').toggleClass('is-visible', bool);
		$('main').toggleClass('scale-down', bool);
	}
	
	// Smooth scroll for anchors
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html,body').animate({
			  scrollTop: target.offset().top
			}, 1000);
			return false;
		  }
		}
	  });
	  

	  var $blocks = $('.employment-history__job.notViewed');
	  var $window = $(window);
	
	  $window.on('scroll', function(e){
		$blocks.each(function(i,elem){
		  if($(this).hasClass('viewed')) 
			return;
			
		  isScrolledIntoView($(this));
		});
	  });
	
	/* http://stackoverflow.com/a/488073/477958 */
	function isScrolledIntoView(elem) {
	  var docViewTop = $(window).scrollTop();
	  var docViewBottom = docViewTop + $(window).height();
	  var elemOffset = 0;
	  
	  if(elem.data('offset') != undefined) {
		elemOffset = elem.data('offset');
	  }
	  var elemTop = $(elem).offset().top;
	  var elemBottom = elemTop + $(elem).height();
	  
	  if(elemOffset != 0) { // custom offset is updated based on scrolling direction
		if(docViewTop - elemTop >= 0) {
		  // scrolling up from bottom
		  elemTop = $(elem).offset().top + elemOffset;
		} else {
		  // scrolling down from top
		  elemBottom = elemTop + $(elem).height() - elemOffset
		}
	  }
	  
	  if((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
		// once an element is visible exchange the classes
		$(elem).removeClass('notViewed').addClass('viewed');
		
		var animElemsLeft = $('.employment-history__job.notViewed').length;
		if(animElemsLeft == 0){
		  // with no animated elements left debind the scroll event
		  $(window).off('scroll');
		}
	  }
	}
	
	
	particlesJS("particles-js", {
	  "particles": {
		"number": {
		  "value": 43,
		  "density": {
			"enable": true,
			"value_area": 800
		  }
		},
		"color": {
		  "value": "#8e8659"
		},
		"shape": {
		  "type": "circle",
		  "stroke": {
			"width": 0,
			"color": "#000000"
		  },
		  "polygon": {
			"nb_sides": 5
		  },
		  "image": {
			"src": "img/github.svg",
			"width": 100,
			"height": 100
		  }
		},
		"opacity": {
		  "value": 0.5,
		  "random": true,
		  "anim": {
			"enable": false,
			"speed": 1,
			"opacity_min": 0.1,
			"sync": false
		  }
		},
		"size": {
		  "value": 10,
		  "random": true,
		  "anim": {
			"enable": false,
			"speed": 40,
			"size_min": 0.1,
			"sync": false
		  }
		},
		"line_linked": {
		  "enable": false,
		  "distance": 500,
		  "color": "#ffffff",
		  "opacity": 0.4,
		  "width": 2
		},
		"move": {
		  "enable": true,
		  "speed":1,
		  "direction": "bottom",
		  "random": false,
		  "straight": false,
		  "out_mode": "out",
		  "bounce": false,
		  "attract": {
			"enable": false,
			"rotateX": 600,
			"rotateY": 1200
		  }
		}
	  },
	  "interactivity": {
		"detect_on": "canvas",
		"events": {
		  "onhover": {
			"enable": true,
			"mode": "bubble"
		  },
		  "onclick": {
			"enable": true,
			"mode": "repulse"
		  },
		  "resize": true
		},
		"modes": {
		  "grab": {
			"distance": 400,
			"line_linked": {
			  "opacity": 0.5
			}
		  },
		  "bubble": {
			"distance": 400,
			"size": 4,
			"duration": 0.3,
			"opacity": 1,
			"speed": 3
		  },
		  "repulse": {
			"distance": 200,
			"duration": 0.4
		  },
		  "push": {
			"particles_nb": 4
		  },
		  "remove": {
			"particles_nb": 2
		  }
		}
	  },
	  "retina_detect": true
	});

	
});