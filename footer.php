           
                <footer role='contentinfo'>
                    <div class='wrapper'>
                        <?php include 'parts/social.php'; ?>
                        <div class='credits'>
                            <p>&copy; Annemarie Parsons 2014</p>
                            <!--<ul class='footer-links'>
                                <li><a href='#'>Privacy Policy and Cookie Information</a></li>
                                <li><a href='terms-of-business.php'>Terms of Business</a></li>
                            </ul>-->
                        </div>
                    </div>
                </footer>
                
        	</div> <!-- end .cd-section -->
        </div> <!-- end .cd-content -->
    </main>
	<?php include 'parts/mobile-menu.php'; ?>
</body>
</html>
