<section class='responsive'>
    <div class='wrapper'>
        <div class='grid'>
            <div class='grid__item'>
                <h2><span>&lt;</span> responsive <span>&gt;</span></h2>
                <p>I specialise in Responsive Front–End Builds and Wordpress theme development using Gulp, SASS, jQuery, PHP and HTML.</p>
            </div>
        </div>
    </div>
</section>