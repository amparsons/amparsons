<ul class='social'>
    <li class='social__item social__item--twitter'>
        <a href='https://twitter.com/tweetingFrog' target='_blank' class='social__item__link' aria-label='twitter'></a>
    </li>
    <li class='social__item social__item--linkedin'>
        <a href='http://uk.linkedin.com/pub/annemarie-parsons/23/423/509' target='_blank' class='social__item__link' aria-label='linkedin'></a>
    </li>
    <li class='social__item social__item--flickr'>
        <a href='http://www.flickr.com/photos/78393564@N06/' target='_blank' class='social__item__link' aria-label='flickr'></a>
    </li>
</ul>