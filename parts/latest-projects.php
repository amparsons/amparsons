<section class='add-padding'>  
    <a name='work' aria-label='anchor' alt='anchor'></a>
    <h2><span>&lt;//</span> latest project <span>&gt;</span></h2>
    <div class='latest-project-container'>
        <div class='cycle-slideshow latest-project'
            data-cycle-swipe=true
            data-cycle-timeout='8000'
            data-cycle-fx='scrollHorz' 
            data-cycle-slides='> div.latest-project__slide'
            data-cycle-prev='.latest-project__prev'
            data-cycle-next='.latest-project__next'
            >
            <div class='latest-project__slide'>
                <div class='latest-project__slide__container'>
                    <img class='latest-project__slide__image' src='img/work/slide1-small.jpg'>
                    <div class='latest-project__mask'>  
                        <h2 class='latest-project__project-title'>DermaSkin</h2>  
                        <p class='latest-project__project-type'>Freelance</p>  
                        <a class='latest-project__project-link' href='http://www.dermaskin.co.uk/' target='_blank'>view site</a>  
                    </div>
                </div>
            </div>
            <div class='latest-project__slide'>
                <div class='latest-project__slide__container'>
                    <img class='latest-project__slide__image' src='img/work/slide3-small.jpg'>
                    <div class='latest-project__mask'>  
                        <h2 class='latest-project__project-title'>Cofactor Science</h2>  
                        <p class='latest-project__project-type'>Freelance</p>  
                        <a class='latest-project__project-link' href='http://cofactorscience.com/' target='_blank'>view site</a>  
                    </div>
                </div>
            </div>
            <div class='latest-project__slide'>
                <div class='latest-project__slide__container'>
                    <img class='latest-project__slide__image' src='img/work/slide4-small.jpg'>
                    <div class='latest-project__mask'>  
                        <h2 class='latest-project__project-title'>WEN Wales</h2>  
                        <p class='latest-project__project-type'>Freelance</p>  
                        <a class='latest-project__project-link' href='http://www.wenwales.org.uk/' target='_blank'>view site</a>  
                    </div>
                </div>
            </div>
            <div class='latest-project__slide'>
                <div class='latest-project__slide__container'>
                    <img class='latest-project__slide__image' src='img/work/slide2-small.jpg'>
                    <div class='latest-project__mask'>  
                        <h2 class='latest-project__project-title'>Cardiff Bay Dental</h2>  
                        <p class='latest-project__project-type'>Freelance</p>  
                        <a class='latest-project__project-link' href='http://www.cardiffbaydental.co.uk/' target='_blank'>view site</a>  
                    </div>
                </div>
            </div>
            <div class='latest-project__next'>
                <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
                xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
                enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
                <path fill='#594857' d='M0,30l0-5.8l22.3-9.2L0,5.8L0,0l28,12.2v5.6L0,30z'/>
                </svg>
            </div>
            <div class='latest-project__prev'>
                <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
                xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
                enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
                <path fill='#594857' d='M0,17.8v-5.6L28,0v5.8L5.7,15.1L28,24.2V30L0,17.8z'/>
                </svg>
            </div>
		</div>
	</div>
</section>      