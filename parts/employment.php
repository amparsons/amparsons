<section class='employment add-padding'>
    <div class='wrapper'>
        <div class='grid'>
            <div class='grid__item'>
            	<a name='cv'></a>
                <h2><span>&lt;//</span> employment <span>&gt;</span></h2>
                <ul class='employment-history'>
                	<li class='employment-history__item'>
                    	<span class='employment-history__date'>2012</span>
                        <div class='employment-history__job notViewed'  data-position='left'>
                        	<h1 class='employment__job__title'>Freelance Front–End Web Developer</h1>
                            <p class='employment__job__description'>I have been working as a freelancer mainly for digital agencies since May 2012 and love it! I have built up some great working relationships which supply me with regular development work. I am always available to chat about new work and exciting development opportunities.</p>
                        </div>
                    </li>
                    <li class='employment-history__item'>
                    	<span class='employment-history__date employment-history__date--right'>2009</span>
                        <div class='employment-history__job notViewed' data-position='right'>
                        	<h1>Web Developer</h1>
                            <p>Employment – bluegg.co.uk</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>