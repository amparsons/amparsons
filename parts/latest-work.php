<section class='add-padding'>
    <a name='work'></a>
	<h2><span>&lt;//</span> latest project <span>&gt;</span></h2>
    <div class='cycle-slideshow-container'>
        <div class='cycle-slideshow latest-work'
            data-cycle-timeout='7000'
            data-cycle-fx='scrollHorz' 
            data-cycle-slides='> div.latest-work__slide'
            data-cycle-prev='.latest-work__prev'
            data-cycle-next='.latest-work__next'
            >
            <div class='latest-work__slide'>
                <div class='latest-work__slide__container'>
                    <img class='latest-work__slide__image' src='img/work/slide1-small.jpg'>
                </div>
            </div>
            <div class='latest-work__slide'>
                <div class='latest-work__slide__container'>
                    <img class='latest-work__slide__image' src='img/work/slide2-small.jpg'>
                </div>
            </div>
            <div class='latest-work__slide'>
                <div class='latest-work__slide__container'>
                    <img class='latest-work__slide__image' src='img/work/slide3-small.jpg'>
                </div>
            </div>
            <div class='latest-work__slide'>
                <div class='latest-work__slide__container'>
                    <img class='latest-work__slide__image' src='img/work/slide4-small.jpg'>
                </div>
            </div>
            <div class='latest-work__next'>
                <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
                xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
                enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
                <path fill='#594857' d='M0,30l0-5.8l22.3-9.2L0,5.8L0,0l28,12.2v5.6L0,30z'/>
                </svg>
            </div>
            <div class='latest-work__prev'>
                <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
                xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
                enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
                <path fill='#594857' d='M0,17.8v-5.6L28,0v5.8L5.7,15.1L28,24.2V30L0,17.8z'/>
                </svg>
            </div>
		</div>
	</div>

    <div class='cycle-slideshow background latest-work--text'
        data-cycle-swipe=true
        data-cycle-timeout='7000'
        data-cycle-fx='scrollHorz' 
        data-cycle-slides='> div.latest-work--text__slide'
        data-cycle-prev='.latest-work__prev'
        data-cycle-next='.latest-work__next'
        >
        <div class='latest-work--text__slide'>
            <h1 class='latest-work--text__slide__title'>DermaSkin</h1>
            <h2 class='latest-work--text__slide__type'>Freelance</h2> 
        	<div class='latest-work__details'>
            	<p class='latest-work__details__title'>Designer: Liam</p>
                <p class='latest-work__details__fonts'>Custom fonts: TypeKit</p>
                <p class='latest-work__details__description'>Code, Open Source used:<br>HTML5, CSS3, jQuery Library, WordPress CMS</p>
            </div>
            <a class='latest-work--text__slide__button' href='http://www.dermaskin.co.uk/' target='_blank'>view site</a>
        </div>
        <div class='latest-work--text__slide'>
            <h1 class='latest-work--text__slide__title'>Cardiff Bay Dental</h1>
            <h2 class='latest-work--text__slide__type'>Freelance</h2> 
            <div class='latest-work__details'>
            	<p class='latest-work__details__title'>Designer: Liam</p>
                <p class='latest-work__details__fonts'>Custom fonts: TypeKit</p>
                <p class='latest-work__details__description'>Code, Open Source used:<br>HTML5, CSS3, jQuery Library, WordPress CMS</p>
            </div>
            <a class='latest-work--text__slide__button' href='http://www.cardiffbaydental.co.uk/' target='_blank'>view site</a>
        </div>
        <div class='latest-work--text__slide'>
            <h1 class='latest-work--text__slide__title'>Cofactor Science</h1>
            <h2 class='latest-work--text__slide__type'>Contract</h2> 
            <div class='latest-work--text__slide__details'>
            	<p class='latest-work__details__title'>Agency: Lean Digital</p>
                <p class='latest-work__details__fonts'>Custom fonts: TypeKit</p>
                <p class='latest-work__details__description'>Code, Open Source used:<br>HTML5, CSS3, jQuery Library, WordPress CMS</p>
            </div>
            <a class='latest-work--text__slide__button' href='http://cofactorscience.com/' target='_blank'>view site</a>
        </div>
        <div class='latest-work--text__slide'>
            <h1 class='latest-work--text__slide__title'>WEN Wales</h1>
            <h2 class='latest-work--text__slide__type'>Freelance</h2> 
            <div class='latest-work--text__slide__details'>
            	<p class='latest-work__details__title'>Designer: Huw David Design</p>
                <p class='latest-work__details__fonts'>Custom fonts: Font-face</p>
                <p class='latest-work__details__description'>Code, Open Source used:<br>HTML5, CSS3, jQuery Library, WordPress CMS</p>
            </div>
            <a class='latest-work--text__slide__button' href='http://www.wenwales.org.uk/' target='_blank'>view site</a>
        </div>
    </div>
</section>      