<section class='availability'>
    <div class='wrapper'>
        <div class='grid'>
            <div class='grid__item'>
                <h2><span>&lt;</span> availability <span>&gt;</span></h2>
                <p>Available for bookings!</p>
            </div>
        </div>
    </div>
</section>