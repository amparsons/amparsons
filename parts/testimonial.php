<section class='testimonials'>  
    <a name='testimonials'></a>
    <div class='cycle-slideshow-container'>
        <div class='cycle-slideshow testimonial'
            data-cycle-swipe=true
            data-cycle-timeout='4000'
            data-cycle-fx='scrollHorz' 
            data-cycle-auto-height='container'
            data-cycle-slides='> div.testimonial__slide'
            data-cycle-prev='.testimonial__prev'
            data-cycle-next='.testimonial__next'
            >
            <div class='testimonial__slide'>
                <div class='details'>
                    <h1>Chris Plummer:</h1>
                    <p>Responsive website designer / <br>accessibility consultant</p>
                </div>
                <div class='triangle top'>
                    <svg class='quote-top-left' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                            C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                            s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                    </g>
                    </svg>
                    <p>Annemarie is a pleasure to work with and has delivered consistently high quality work in a very professional and personable manner. Having worked with Annemarie on detailed and complex website developments I am impressed with her expertise, skill and attention to detail.</p>
                    <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                            c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                            c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                    </g>
                    </svg>
                </div>
            </div>
            <div class='testimonial__slide'>
                <div class='details'>
                    <h1>Sion Ashley-Jones:</h1>
                    <p>Experienced Brand &amp; Marketing<br>Communications professional</p>
                </div>
                <div class='triangle top'>
                    <svg class='quote-top-left' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28"  xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                            C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                            s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                    </g>
                    </svg>
                    <p>Annemarie is without doubt an expert in her field of web design, development and production. She has never failed to deliver at any stage and she has always gone the extra mile which has been a massive help throughout the process. I would have no hesitation to either work with her her again or to recommend her, in fact I hope to develop a working relationship with her and put a few jobs in her direction! Anyone who works with Annemarie will without doubt have both a rewarding and successful experience.</p>
                    <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                            c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                            c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                    </g>
                    </svg>
                </div>
            </div>
		
        <div class='testimonial__slide'>
                <div class='details'>
                    <h1>Kevin Ogden:</h1>
                    <p>Managing Director<br/>Horizon Digital Media</p>
                </div>
                <div class='triangle top'>
                    <svg class='quote-top-left' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28"  xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                            C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                            s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                    </g>
                    </svg>
                    <p>Anne-marie has provided our web department with excellent support. Her knowledge and expertise of web design and IT is very impressive and I would have no hesitation whatsoever in recommending her services to anyone who may have skill shortages in this area of their business.</p>
                    <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                            c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                            c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                    </g>
                    </svg>
                </div>
            </div>
		
        <div class='testimonial__slide'>
                <div class='details'>
                    <h1>Darren Joslin:</h1>
                    <p>Print, Design, Online<br/>Horizon Digital Media</p>
                </div>
                <div class='triangle top'>
                    <svg class='quote-top-left' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28"  xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                            C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                            s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                    </g>
                    </svg>
                    <p>I commissioned Annemarie to work on a number of web projects over the past 3 years and can fully recommend her for her quality of work, professional approach and problem solving ability. Annemarie is a very talented designer, developer and can also work on server side issues. I will continue to use Annemarie's services.</p>
                    <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#fff" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                            c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                            c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                    </g>
                    </svg>
                </div>
          
		</div>
         <div class='testimonial__next'>
          <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
          xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
          enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
          <path fill='#594857' d='M0,30l0-5.8l22.3-9.2L0,5.8L0,0l28,12.2v5.6L0,30z'/>
          </svg>
      </div>
      <div class='testimonial__prev'>
          <svg version='1.1' id='Layer_1' xmlns:x='&ns_extend;' xmlns:i='&ns_ai;' xmlns:graph='&ns_graphs;'
          xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 28 30'
          enable-background='new 0 0 28 30' width='28' height='30' xml:space='preserve'>
          <path fill='#594857' d='M0,17.8v-5.6L28,0v5.8L5.7,15.1L28,24.2V30L0,17.8z'/>
          </svg>
      </div>
	</div>
   
</section>      