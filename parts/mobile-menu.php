<nav class='cd-nav-container' id='cd-nav'>
    <a href='#' class="cd-close-nav">Close</a>
    <ul class='cd-nav'>
        <li class="cd-selected" data-menu="index">
            <a href='index.php'>
            	<svg class="nc-icon outline" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="44px" height="44px" viewBox="0 0 64 64">
                <g transform="translate(0, 0)"> <polyline data-cap="butt" fill="none" stroke="#3c2d39" stroke-width="2" stroke-miterlimit="10" points="10,24.9 10,60 26,60 26,44 38,44 38,60 54,60 54,24.9 " stroke-linejoin="square" stroke-linecap="butt"></polyline> 
                <polyline data-color="color-2" fill="none" stroke="#3c2d39" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 4,30 32,6 60,30 " stroke-linejoin="square"></polyline> <rect data-color="color-2" x="26" y="24" fill="none" stroke="#3c2d39" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" width="12" height="10" stroke-linejoin="square"></rect> </g>
                </svg>
            </a>
        </li>
        <li data-menu='about-us'>
            <a href='about.php'>About</a>
        </li>
        <li data-menu='work'>
            <a href='work.php'>Work</a>
        </li>
        <li data-menu='testimonials'>
            <a href='testimonials.php'>Testimonials</a>
        </li>
        <li data-menu='contact'>
            <a href='contact.php'>Contact</a>
        </li>
        <!-- other list items here -->
    </ul> <!-- .cd-3d-nav -->
</nav>