<section class='skills'>
	<div class='wrapper'>
        <div class='grid'>
            <div class='grid__item'>
                <h2><span>&lt;</span> skills <span>&gt;</span></h2>
                <p>I am constantly developing my skills and looking at new ways to develop sites. At the moment I am looking to learn custom coding jQuery, Touch events and SuperCPT for Wordpress.</p>
            </div>
            <div class='wrapper-inner'>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon1.svg" alt="Icon name"/>
                        <p class='skill__name'>Dreamweaver, Fireworks, Photoshop &amp; Illustrator.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon2.svg" alt="Icon name"/>
                        <p class='skill__name'>CSS/HTML Compatible with IE9+ standard compliant browsers CSS3 Media Queries Mobile first responsive coding using the Modular approach with CSS Wizards Grid.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon3.svg" alt="Icon name"/>
                        <p class='skill__name'>I use SASS  on all of my development projects.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon4.svg" alt="Icon name"/>
                        <p class='skill__name'>I use CSS Wizardry Grids or Bourbon Neat for those grid layouts.</p>
                    </div>
                </div>
                
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon5.svg" alt="Icon name"/>
                        <p class='skill__name'>HAML and Twig Template Engine - Good working knowledge.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon6.svg" alt="Icon name"/>
                        <p class='skill__name'>Theme building, Custom post type design, Custom taxonomies.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon7.svg" alt="Icon name"/>
                        <p class='skill__name'>I use BitBucket for my repository with Tower app or command line management.</p>
                    </div>
                </div>
                <div class='grid__item max-tablet--one-half desktop--one-quarter'>
                    <div class='skill'>
                        <img class='skill__icon' src="img/icons/icon8.svg" alt="Icon name"/>
                        <p class='skill__name'>Integration of styling into .NET pages and MVC frameworks. Time reports available on request generated using FreeAgent.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>