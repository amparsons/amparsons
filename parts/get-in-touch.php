<?php
$action = '';
if ((isset($_POST['fname'])) && ($_POST['username'] == ''))
{
	$msg = $_POST['message'];
	if ((strlen($msg) == strlen(strip_tags($msg))) && (strpos($msg,"http") === false) && (strpos($msg,"[url]") === false))
	{
	
		//Get template for email
		$textEmail = file_get_contents("emails/contact.txt");
		
		$textEmail = str_replace("|FNAME|",$_POST['fname'],$textEmail);
		$textEmail = str_replace("|TEL|",$_POST['tel'],$textEmail);
		$textEmail = str_replace("|EMAIL|",$_POST['email'],$textEmail);
		$textEmail = str_replace("|BUDGET|",$_POST['budget'],$textEmail);
		$textEmail = str_replace("|MESSAGE|",$_POST['message'],$textEmail);
			
		$to = "annemarie@amparsons.co.uk";
		$subject = "Contact from amparsons";
		$headers = "From: annemarie@amparsons.co.uk"."\r\n"."X-Mailer: PHP/".phpversion();
		
		
		ini_set("sendmail_from", " annemarie@amparsons.co.uk ");
		mail($to,$subject,$textEmail,$headers, '-fannemarie@amparsons.co.uk');
		
		$action = 'sent';
		
	}
}
?>
<section class='get-in-touch'>
  <div class='wrapper'>
      <?php if ($action !== 'sent') { ?>
      <h2><span>&lt;</span> get in touch <span>&gt;</span></h2>
      <p>I am based in Penarth and would love to meet up for a coffee. I hold most of my meetings at my local! To find Ocho Lounge <a href='https://www.google.co.uk/maps/place/Ocho+Lounge/@51.439116,-3.17444,15z/data=!4m2!3m1!1s0x0:0x841e25d695d0cd18?sa=X&ei=DBFrVOyOIoz7apmGgcAE&ved=0CIwBEPwSMAs' target='_blank'>click here</a>.</p>
      <form name='contact' action='<?php echo $_SERVER['PHP_SELF']?>' method='post'>
          <fieldset>
              <input name='fname' type='text' placeholder='Your Name' required>
              <input name='tel' type='text' placeholder='Your Telephone No. (Optional)'>
              <input name='email' type='email' placeholder='Your Email Address' required>
              <div class='select-style'>
                  <select name='budget'>
                      <option disabled selected>What is your budget?</option>
                      <option>< &pound;1000</option>
                      <option>> &pound;1000</option>
                      <option>> &pound;2000</option>
                      <option>> &pound;3000</option>
                  </select>
              </div>
          </fieldset>
          <fieldset>
              <textarea name='message' placeholder='Your Message' required></textarea>
              <input name='username' type='hidden' value='' />
          </fieldset>
          <input type='submit' value='Submit'>
      </form>
      <?php } else { ?>
      	<h2><span>&lt;</span> Thank you <span>&gt;</span></h2
        ><p>Your message is winging its way to me. I will be in touch to discuss your enquiry. Look forward to chatting.</p>
      <?php } ?>
  </div>
</section>