<section class='current-role'>
    <div class='wrapper'>
        <div class='grid'>
            <div class='grid__item'>
                <h2><span>&lt;</span> current role <span>&gt;</span></h2>
                <p>I work as a freelance Front–End Developer for a number of digital agencies and<br>freelance designers all over the UK.</p>
            </div>
        </div>
    </div>
</section>