<?php include 'header.php'; ?>
	
    <section class='selected-works'>
        <div class='wrapper'>
            <div class='grid'>
                <div class='grid__item'>
                    <h1>selected works</h1>
                    <div class='introduction'>
                    	<h2>work feed</h2>
                    	<p><q>I code in HTML5, SASS, jQuery and use Gulp as my task runner. I have both GIT and Bitbucket accounts.</q></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
  	<?php include 'parts/latest-work.php'; ?>
    <?php include 'parts/get-in-touch.php'; ?>
<?php include 'footer.php'; ?>