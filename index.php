<?php include 'header.php'; ?>

	<header role='banner' id='particles-js'>
    	<!--<div class='coffee-cup'></div>
        <div class='phone'></div>
        <div class='pen'></div>
        <div class='ipad'></div>-->
        <div class='wrapper'>
            <div class="banner">
                <div class='header-welcome'>
                    <h2>hello</h2>
                    <p>I am a Front-End/Wordpress Developer. Working with digital agencies.</p>
                    <p class='yellow'>Available for bookings!</p>
                </div>
                <a class='arrow' href='#work' aria-label="anchor">
                    <svg version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 28"
                    enable-background="new 0 0 30 28" width="30" height="28"  xml:space="preserve">
                    <path fill="#594857" d="M0,0h5.8l9.2,22.3L24.2,0H30L17.8,28h-5.6L0,0z"/>
                    </svg>
                </a>
            </div>
        </div>
    </header>
    <?php include 'parts/latest-projects.php'; ?>
    <?php include 'parts/responsive.php'; ?>
    <?php include 'parts/get-in-touch.php'; ?>
<?php include 'footer.php'; ?>