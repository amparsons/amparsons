<?php include 'header.php'; ?>
    <section class='contact'>
        <div class='wrapper'>
            <div class='grid'>
                <div class='grid__item tablet--eight-twelfths push--tablet--two-twelfths'>
                    <div class='triangle dark bottom full-width'>
                    	<svg class='quote-top-left' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                             enable-background="new 0 0 36 28"  xml:space="preserve">
                        <g>
                            <path fill="#e0dc00" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                                C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                                s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                        </g>
                        </svg>
                        <h1>feel free to<br>contact me./</h1>
                        <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                             enable-background="new 0 0 36 28" xml:space="preserve">
                        <g>
                            <path fill="#e0dc00" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                                c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                                c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                        </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'parts/get-in-touch.php'; ?>
<?php include 'footer.php'; ?>