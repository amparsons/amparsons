<?php
	$activepage = ''; 
	if (strpos($_SERVER['PHP_SELF'], 'about.php')) { 
		$activepage = 'class="about brown"';
	} elseif (strpos($_SERVER['PHP_SELF'], 'work.php')) {
		$activepage = 'class="work brown"';
	} elseif (strpos($_SERVER['PHP_SELF'], 'testimonials.php')) {
		$activepage = 'class="testimonail-page brown"';
	} elseif (strpos($_SERVER['PHP_SELF'], 'contact.php')) {
		$activepage = 'class="contact"';
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class='no-js lt-ie9'> <![endif]-->
<!--[if gt IE 8]><!--> <html class='no-js'> <!--<![endif]-->
<head>
	<meta charset='utf-8'>
	<title>Annemarie Parsons | Freelance Front-End Web Developer Cardiff, Bristol, London, Newcastle</title>
    <meta name="description" content="Freelance Front-End Web Developer Cardiff, Bristol, London, Newcastle" />
  	<meta name="keywords" content="freelance, web developer, front end developer, websites, design, wordpress developer, cardiff, penarth, lodnon, newcastle">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Annemarie Parsons | amparsons">
    <meta itemprop="description" content="I am a Front-End / Wordpress Developer. Working with digital agencies.">
    
    <!-- for Facebook -->          
    <meta property="og:title" content="Annemarie Parsons | amparsons" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="http://www.amparsons.co.uk/img/placeholder.png" />
    <meta property="og:url" content="http://www.amparsons.co.uk" />
    <meta property="og:description" content="I am a Front-End / Wordpress Developer. Working with digital agencies." />
    
    <!-- Twitter Meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@tweetingfrog">
    <meta name="twitter:creator" content="@tweetingfrog">
    <meta name="twitter:title" content="Annemarie Parsons | amparsons">
	
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	<link rel='shortcut icon' href='favicon.png' />
	<link rel="stylesheet" type="text/css" href="style.css">
    
    <script src="//use.typekit.net/jmq4eph.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 	<script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script type='text/javascript' src='plugins.min.js'></script>
    <script type='text/javascript' src='scripts.min.js'></script>
    
     <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-31642661-1', 'auto');
	  ga('send', 'pageview');
	 
	</script>
</head>

<body <?php echo $activepage; ?>>
	<a href="#cd-nav" class="cd-nav-trigger">
		Menu<span><!-- used to create the menu icon --></span>
	</a> <!-- .cd-nav-trigger -->
	<main>
		<div class="cd-section index cd-selected">
			<div class="cd-content">
                <div class="navigation-common">
                    <div class='wrapper'>
                        <h1 class='logo'><a href='/'>annemarie<br>parsons</a></h1>
                        <?php include 'parts/menu.php'; ?>
                    </div>
                </div>