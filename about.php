<?php include 'header.php'; ?>
	
    <section class='challenge'>
        <div class='wrapper'>
            <div class='grid'>
                <div class='grid__item'>
                    <svg class='quote-top-left' version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 36 28" enable-background="new 0 0 36 28"  xml:space="preserve">
                    <g>
                        <path fill="#3C2D39" d="M9.7,0l5.2,4.2c-3,1.6-6.5,5.6-7.4,9c0.4-0.2,1.1-0.3,1.9-0.3c3.6,0,6.4,2.7,6.4,6.8s-3.4,7.5-7.5,7.5
                            C4,27.2,0,23.7,0,17.5C0,10.5,3.9,4.2,9.7,0z M29,0l5.2,4.2c-3,1.6-6.6,5.6-7.4,9c0.3-0.2,1.1-0.3,1.9-0.3c3.6,0,6.3,2.7,6.3,6.8
                            s-3.3,7.5-7.4,7.5c-4.4,0-8.4-3.5-8.4-9.6C19.2,10.5,23.1,4.2,29,0z"/>
                    </g>
                    </svg>
                    <h1>i love the challenge</h1>
                    <p>I create responsive content managed websites using HTML5, CSS3/SASS, jQuery, PHP and MySQL.</p>
                    <svg class='quote-top-right' version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 36 28"
                         enable-background="new 0 0 36 28" xml:space="preserve">
                    <g>
                        <path fill="#3C2D39" d="M6.3,27.9L1,23.6c3.1-1.6,6.7-5.7,7.5-9.2c-0.3,0.2-1.1,0.3-1.8,0.3c-3.7,0-6.6-2.8-6.6-7S3.5,0,7.7,0
                            c4.6,0,8.6,3.6,8.6,9.9C16.3,17.2,12.3,23.7,6.3,27.9z M26,27.9l-5.3-4.4c3.1-1.6,6.8-5.7,7.6-9.2c-0.3,0.2-1.2,0.3-1.9,0.3
                            c-3.7,0-6.5-2.8-6.5-7S23.2,0,27.4,0C31.9,0,36,3.6,36,9.9C36,17.2,32,23.7,26,27.9z"/>
                    </g>
                    </svg>
                    <a class='arrow' href='#cv'>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 30 28" enable-background="new 0 0 30 28" width="30" height="28"  xml:space="preserve">
                        	<path fill="#594857" d="M0,0h5.8l9.2,22.3L24.2,0H30L17.8,28h-5.6L0,0z"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php include 'parts/employment.php'; ?>
    <?php include 'parts/skills.php'; ?>
    <?php include 'parts/current-role.php'; ?>
    <?php include 'parts/availability.php'; ?>
    <?php include 'parts/get-in-touch.php'; ?>
<?php include 'footer.php'; ?>